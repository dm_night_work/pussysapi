## PussysAPI - RESTfull API template/boilerplate
Template/Boilerplate for RESTfull API with Laravel and AngularJS.
Initiali developed with Laravel 5.1 and AngularJS 1.45.

#### INSTALL
1. Install composer globally
2. Clone repo
3. Run `composer install` or you may need `sudo composer install` - this will install latest dependencies in _vendor_ folder
4. Change permissions on _storage_ and _bootstrap/cache_ folder
    * `sudo chown -R www-data:www-data storage/ bootstrap/cache/` - this is for linux(debian based), check HOW TO for your system
    * `sudo chmod -R 755 storage/ bootstrap/cache/`
5. Create __VHOST__ route to _public/_ folder - index.php will do the rest of job (recommended for development env: 'pussysapi.lan')
6. Run `npm install --global gulp` or `sudo npm install --global gulp` or install any other task manager and configure it properly
7. Run `npm install` or `sudo npm install` - to install the node modules

#### Setup
1. Copy _.env.example_ to _.env_
2. Edit/Configure _.env_ file to suit your needs
3. Run `php artisan key:generate` or `sudo php artisan key:generate`
4. Run `php artisan migrate` or `sudo php artisan migrate` to build DB tabels

You should be able to see front-page if you navigate to host you set up