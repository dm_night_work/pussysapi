<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Pussy as PussyModel;

class PussyController extends Controller
{
    protected $pussy = 1;
    protected $spacer = '-';

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        // angular QUERY request
        return PussyModel::getPussies();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return int
     */
    public function store(Request $request)
    {
        // angular SAVE request
        $pussy = new PussyModel();

        $pussy->pussy = $request->input('pussy');
        $pussy->size = $request->input('size');
        $pussy->sketch = $this->sketching($request->input('size'), $this->spacer)['pussy'];
        $pussy->active = $request->input('active');

        if ($pussy->save()) { return 1; }
        else { return 0; }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //get row from DB according to $id
        if (is_numeric($id)) {
           $data = $this->sketching($id, $this->spacer);
            return $data;
        }

        return PussyModel::getPussyByName($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return int
     */
    public function update(Request $request, $id)
    {
        if (is_numeric($id)) {
            $pussy = PussyModel::find($id);

            $pussy->pussy = $request->input('pussy');
            $pussy->size = $request->input('size');
            $pussy->sketch = $this->sketching($request->input('size'), $this->spacer)['pussy'];
            $pussy->active = $request->input('active');

            if ($pussy->save()) { return 1; }
            else { return 0; }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        /*$pussy = PussyModel::find($id);

        if ($pussy->delete()) { return 1; }
        else { return 0; }*/

        return 1;
    }

    /**
     * Create sketch of pussy based on size given.
     *
     * @param $number
     * @param $spacer
     * @return array
     */
    private function sketching($number, $spacer) {
        $sketch = array(
            'pussy' => '{{'
        );

        for ($i = 0; $i < ($number * 2); ++$i) {
            if ($i == $number) { $sketch['pussy'] .= '||' . $spacer; }
            else { $sketch['pussy'] .= $spacer; }
        }

        $sketch['pussy'] .= '}}';

        return $sketch;
    }
}
