<?php
/**
 * Created by PhpStorm.
 * User: fly
 * Date: 10/07/15
 * Time: 13:51
 */

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pussy extends Model
{
    protected $dates = ['deleted_at'];
    protected $queue = 12;
    protected $casts = [
        'active' => 'integer'
    ];

    public function scopeGetPussies($query) {
        $data = $query->orderBy('created_at', 'DESC')
                      ->take($this->queue)->get();

        return $data;
    }

    public function scopeGetPussyByName($query, $name) {
        if (isset($name) && !empty($name)) {
            $data = $query->where('pussy', 'LIKE', '%' . $name . '%')
                          ->get();

            return $data;
        }

        return false;
    }
}