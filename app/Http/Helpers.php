<?php
/**
 * Created by PhpStorm.
 * User: fly
 * Date: 03/08/15
 * Time: 19:38
 */

namespace App\Http;

class Helpers {

    public static function getClientId() {
        return md5(getenv("REMOTE_ADDR").'webdev'.getenv('HTTP_USER_AGENT'));
    }

}