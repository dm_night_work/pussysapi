<?php use App\Http\Helpers; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>PussysAPI</title>
        <meta content="True" name="HandheldFriendly">
        <meta content="320" name="MobileOptimized">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta name="description" content="PussysAPI is main API for pussys! Get some!">
        <meta name="keywords" content="pussysapi, pussys api, restfull, restfullapi, restfull api, development">

        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        @if (!App::environment('local'))
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-65913874-1', {'storage': 'none', 'clientId': '{{ Helpers::getClientId() }}'});
            ga('send', 'pageview', {'anonymizeIp': true});
        </script>
        @endif
    </head>
    <body>
        <div class="app-wrapper">
            <!-- Header -->
            <header class="app-main-header">
                <div class="content-full copy-ready cf">
                    <div class="left">
                        <h1>Pussys API</h1>
                    </div>
                    <div class="header-mobile--icon right">
                        <i id="menu" class="fa fa-bars"></i>
                    </div>
                    <div id="navigation" class="header-main right">
                        <nav>
                            <a id="get-sketch" href="#getsketch">GetImage</a>
                            <a id="get-all" href="#getall">GetAll</a>
                            <a id="save-it" href="#save">SaveIt</a>
                            <a id="get-it" href="#get">Get</a>
                            <a id="put-it" href="#put">Put</a>
                            <a id="delete-it" href="#delete">Delete</a>
                        </nav>
                    </div>
                </div>
            </header>

            <!-- Main -->
            <main class="app-main">
                <!-- ABOUT -->
                <section class="content-full copy-ready">
                    <h1 class="title copy-ready-inner">WHY ?</h1>
                    <div class="copy-ready-inner">
                        <p>This project was created and started in favour of study cases.</p>
                        <p>Later in development stage, we discover that can also use some of the code that is repeating and create boilerplate for RESTfull examples.</p>
                        <p>All examples are connected with 2 main technologies.</p>
                        <p>Built with <strong>Laravel 5.1</strong> and <strong>AngularJS 1.4</strong></p>
                    </div>
                </section>
                <section class="content-full copy-ready">
                    <h1 class="title copy-ready-inner">WHO ?</h1>
                    <div class="copy-ready-inner">
                        <p>Developed and crafted: <strong>Development team in Bosnia</strong>!</p>
                    </div>
                </section>
                <section class="content-full copy-ready">
                    <h1 class="title copy-ready-inner">WHOM ?</h1>
                    <div class="copy-ready-inner">
                        <p>For all enthusiatic developers, who are willing to <em>try</em>, <em>test</em> or <em>comment</em> API</p>
                        <p>Let's get started!</p>
                    </div>
                    <div class="starter-wrapper">
                        <div class="starter">
                            <a href="#start" class="fa fa-arrow-down"></a>
                        </div>
                    </div>
                </section>

                <!-- APP -->
                <div id="start" class="pussyapi-app-wrapper" ng-app="PussyApi">
                    <div ng-view></div>
                </div>
            </main>

            <!-- Footer -->
            <footer class="app-main-footer">
                <section class="content-full copy-ready">
                    <h1>WHATS NEXT ?</h1>
                    <p>New features are comming THIS Fall</p>
                    <p>
                        Made with <i class="fa fa-coffee"></i> and <i class="fa fa-beer"></i>!
                        <small>Small team..</small>
                    </p>
                </section>
            </footer>
        </div>

        <script src="{{ asset('js/script.js') }}"></script>
    </body>
</html>
