/**
 * Created by fly on 03/08/15.
 */
(function(window) {

    /**
     * VARS
     */

    var menuBtn,
        menuNav,
        getSketch,
        getAll,
        saveIt,
        getIt,
        putIt,
        deleteIt,
        startBtn;


    /**
     * EVENTS
     */

    window.addEventListener('load', function() {
        menuBtn   = document.getElementById('menu');
        menuNav   = document.getElementById('navigation');
        getSketch = document.getElementById('get-sketch');
        getAll    = document.getElementById('get-all');
        saveIt    = document.getElementById('save-it');
        getIt     = document.getElementById('get-it');
        putIt     = document.getElementById('put-it');
        deleteIt  = document.getElementById('delete-it');
        startBtn  = document.getElementById('start');

        menuBtn.addEventListener('click', function() {
            menuNav.classList.toggle('active');
        }, false);

        getSketch.addEventListener('click', function() {
            menuFunctionality()
            window.ga('send', 'event', 'button', 'click', 'nav button getSketch');
        }, false);
        getAll.addEventListener('click', function() {
            menuFunctionality()
            window.ga('send', 'event', 'button', 'click', 'nav button getAll');
        }, false);
        saveIt.addEventListener('click', function() {
            menuFunctionality()
            window.ga('send', 'event', 'button', 'click', 'nav button saveIt');
        }, false);
        getIt.addEventListener('click', function() {
            menuFunctionality()
            window.ga('send', 'event', 'button', 'click', 'nav button getIt');
        }, false);
        putIt.addEventListener('click', function() {
            menuFunctionality()
            window.ga('send', 'event', 'button', 'click', 'nav button putIt');
        }, false);
        deleteIt.addEventListener('click', function() {
            menuFunctionality()
            window.ga('send', 'event', 'button', 'click', 'nav button deleteIt');
        }, false);
    }, false);

    /**
     * FUNCTIONS
     */

    function menuFunctionality() {
        startBtn.scrollIntoView();
        window.scrollBy(0, 225);
        menuNav.classList.toggle('active');
    }


})(window);