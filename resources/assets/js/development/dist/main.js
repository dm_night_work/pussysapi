/*! PussysApi (1.35) (C) www.pussysapi.tk .  WTFPL @license: https://en.wikipedia.org/wiki/WTFPL */
/**
 * Created by fly on 10/07/15.
 */
(function(window, angular) {

    var pussyapi;

    pussyapi = angular.module('PussyApi', ['ngResource', 'ngRoute'], ["$interpolateProvider", function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    }]);

    pussyapi.config(['$routeProvider', function($routeProvider) {
        /* Get Sketch */
        $routeProvider.when('/getsketch', {
            templateUrl: 'templates/getsketch.html',
            controller: 'SketchGetController'
        });

        /* GetAll */
        $routeProvider.when('/getall', {
            templateUrl: 'templates/getall.html',
            controller: 'BasicGetController'
        });

        /* Save */
        $routeProvider.when('/save', {
            templateUrl: 'templates/save.html',
            controller: 'BasicPostController'
        });

        /* Get */
        $routeProvider.when('/get', {
            templateUrl: 'templates/get.html',
            controller: 'SpecificGetController'
        });

        /* Put */
        $routeProvider.when('/put', {
            templateUrl: 'templates/put.html',
            controller: 'BasicUpdateController'
        });

        /* Delete */
        $routeProvider.when('/delete', {
            templateUrl: 'templates/delete.html',
            controller: 'BasicDeleteController'
        });
    }]);

    /**
     * CONTROLLERS
     */

    pussyapi.controller('SketchGetController', ['$scope', '$resource', SketchGetController]);

    pussyapi.controller('BasicGetController', ['$scope', '$resource', BasicGetController]);

    pussyapi.controller('BasicPostController', ['$scope', '$resource', BasicPostController]);

    pussyapi.controller('SpecificGetController', ['$scope', '$resource', SpecificGetController]);

    pussyapi.controller('BasicUpdateController', ['$scope', '$resource', 'PussyF', BasicUpdateController]);

    pussyapi.controller('BasicDeleteController', ['$scope', '$resource', 'PussyF', BasicDeleteController]);


    /**
     * FACTORIES
     */

    pussyapi.factory('PussyF', ['$resource', function($resource) {
        return $resource('/pussy/:id/', { id: '@id' }, {
            'update': { method: 'put' },
            'destroy': { method: 'delete' }
        });
    }]);


    /**
     * FUNCTIONS - Controller Functions
     */

    function SketchGetController($scope, $resource) {
        $scope.status = false;
        $scope.info = false;

        $scope.getPussySketch = function(query) {
            var Pussy;

            $scope.pussyGetSketch = {};

            if (!query || typeof query == 'undefined') {
                $scope.info = true;
                return false;
            } else {
                $scope.info = false;
            }

            Pussy = $resource('/pussy/' + query);
            Pussy.get(function(res) {
                if (res.pussy) {
                    $scope.pussyGetSketch.pussy = res.pussy;
                    $scope.status = true;
                }
            });
        };
    }

    function BasicGetController($scope, $resource) {
        $scope.getPussys = function() {
            var Pussy;

            Pussy = $resource('/pussy/');
            Pussy.query(function(res) {
                $scope.response = true;
                if (res[0]) { $scope.pussys = res; }
                else { $scope.response = false; }
            });
        };
    }

    function BasicPostController($scope, $resource) {
        $scope.savePussy = function() {
            var Pussy;

            Pussy = $resource('/pussy/');
            Pussy.save($scope.pussySave, function(res) {
                if (parseInt(res[0])) {
                    $scope.pussySave.reponse = 'Pussy was saved!';
                    $scope.pussySave.pussy = null;
                    $scope.pussySave.size = null;
                    $scope.pussySave.active = null;
                }
                else {
                    $scope.pussySave.response = 'Oops! Pussy wasn\'t saved!'
                }
            });
        };
    }

    function SpecificGetController($scope, $resource) {
        $scope.getPussy = function(query) {
            var Pussy;

            if (!query || typeof query == 'undefined') {
                $scope.pussyGet = {};
                $scope.pussyGet.info = 'Please enter ANY value!';
                return false;
            } else {
                $scope.pussyGet.info = null;
            }

            Pussy = $resource('/pussy/' + query);
            Pussy.query(function(res) {
                if (typeof res[0] == 'undefined') { $scope.pussyGet.empty = true; }
                else { $scope.pussyGet = res[0]; }
            });
        };
    }

    function BasicUpdateController($scope, $resource, PussyF) {
        var Pussy;

        $scope.showPussy = function() {
            Pussy = $resource('/pussy/Danica/');
            Pussy.query(function(res) {
                if (typeof res[0] !== 'undefined') {
                    $scope.pussyLoad = res[0];
                    // small fix, cause AngularJS in combination with <input type="number"> wants data to be int!!
                    $scope.pussyLoad.size = parseInt(res[0].size);
                }
            });
        };

        $scope.updatePussy = function() {
            PussyF.update({ id: $scope.pussyLoad.id }, $scope.pussyLoad, function(res) {
                if (res[0]) { $scope.pussyLoad.response = 'Pussy successfully UPDATED!'; }
                else { $scope.pussyLoad.response = 'There is something wrong with pussy!'; }
            });
        };
    }

    function BasicDeleteController($scope, $resource, PussyF) {
        var Pussy;

        $scope.stinkyPussy = {};

        $scope.getStinkyPussy = function() {
            Pussy = $resource('/pussy/1/');
            Pussy.get(function(res) {
                if (typeof res !== 'undefined') { $scope.stinkyPussy.model = res; }
            });
        };

        $scope.deletePussy = function() {
            PussyF.destroy({ id: 1 }, function(res) {
                if (parseInt(res[0])) {
                    $scope.stinkyPussy.response = 'Stinky is DELETED! Go ADD(post) new one!';
                    $scope.stinkyPussy.model = null;
                }
                else { $scope.stinkyPussy.response = 'There were problems with stinky!'; }
            });
        };
    }

})(window, angular);