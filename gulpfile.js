var gulp = require('gulp'),
    ngAnnotate = require('gulp-ng-annotate'),
    elixir = require('laravel-elixir');

gulp.task('annotate', function() {
    return gulp.src('resources/assets/js/development/main.js')
        .pipe(ngAnnotate())
        .pipe(gulp.dest('resources/assets/js/development/dist'));
});

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less([
       "main.less"
    ], "public/css");

    mix.task('annotate');

    mix.scripts(
        [
            'vendor/angular.min.js',
            'vendor/angular-route.min.js',
            'vendor/angular-resource.min.js',
            'development/dist/main.js',
            'development/tracking.js'
        ], 'public/js/script.js');
});
